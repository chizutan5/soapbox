import React from 'react';

import { render, screen } from 'soapbox/jest/test-helpers';
import { normalizeGroup } from 'soapbox/normalizers';
import { Group } from 'soapbox/types/entities';

import GroupMemberCount from '../group-member-count';

let group: Group;

describe('<GroupMemberCount />', () => {
  describe('without support for "members_count"', () => {
    beforeEach(() => {
      group = normalizeGroup({
        members_count: undefined,
      });
    });

    it('should return null', () => {
      render(<GroupMemberCount group={group} />);

      expect(screen.queryAllByTestId('group-member-count')).toHaveLength(0);
    });
  });

  describe('with support for "members_count"', () => {
    describe('with 1 member', () => {
      beforeEach(() => {
        group = normalizeGroup({
          members_count: 1,
        });
      });

      it('should render correctly', () => {
        render(<GroupMemberCount group={group} />);

        expect(screen.getByTestId('group-member-count').textContent).toEqual('1 member');
      });
    });

    describe('with 2 members', () => {
      beforeEach(() => {
        group = normalizeGroup({
          members_count: 2,
        });
      });

      it('should render correctly', () => {
        render(<GroupMemberCount group={group} />);

        expect(screen.getByTestId('group-member-count').textContent).toEqual('2 members');
      });
    });

    describe('with 1000 members', () => {
      beforeEach(() => {
        group = normalizeGroup({
          members_count: 1000,
        });
      });

      it('should render correctly', () => {
        render(<GroupMemberCount group={group} />);

        expect(screen.getByTestId('group-member-count').textContent).toEqual('1k members');
      });
    });
  });
});